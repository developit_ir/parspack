<?php
/**
 * @OA\Post(
 *      path="/users/login",
 *      tags={"Users"},
 *      summary="login",
 *      description="",
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/LoginRequestBody")
 *     ),
 *     @OA\Response(response=200, description="Successful created", @OA\JsonContent()),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found")
 * )
 */

/**
 * @OA\Post(
 *      path="/users/logout",
 *      tags={"Users"},
 *      summary="logout",
 *      description="",
 *     @OA\Response(response=200, description="Successful created", @OA\JsonContent()),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      *     security={
 *         {
 *             "bearerAuth": {}
 *         }
 *     },
 * )
 */
