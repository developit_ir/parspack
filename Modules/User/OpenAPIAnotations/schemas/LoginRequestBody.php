<?php
namespace Modules\User\OpenAPIAnotations\schemas;
/**
 * @OA\Schema(
 *     description="LoginRequestBody schema model",
 *     title="LoginRequestBody model",
 *     required={"email", "password"}
 * )
 */
class LoginRequestBody
{
    /**
     * @OA\Property(
     *     format="string",
     *     description="",
     *     title="email",
     * )
     *
     * @var string
     */
    private $email;

    /**
     * @OA\Property(
     *     format="string",
     *     description="",
     *     title="password",
     * )
     *
     * @var string
     */
    private $password;
}
