<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * login user by email and password
     * @return \Illuminate\Http\JsonResponse
     */
    public function login() : JsonResponse
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            return response()->json([
                'token' => $user->createToken('user-login-access-token')->accessToken,
            ], 200);
        } else {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email or password you have entered are incorrect.']
                ]
            ], 422);
        }
    }

    public function logout()
    {
        Auth::user()->tokens->each(function($token, $key) {
            $token->delete();
        });

        return response()->json(null, 204);
    }
}
