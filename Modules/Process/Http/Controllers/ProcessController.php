<?php

namespace Modules\Process\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\Process\Process;

class ProcessController extends Controller
{
    /**
     * display a listing of running processes on the server.
     * @return Renderable
     */
    public function index()
    {
        $process = new Process(['ps', '-aux']);
        $process->run();
        return $process->getOutput();
    }
}
