<?php

namespace Modules\Directory\Tests\Unit;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ProcessTest extends TestCase
{
    public $header = ['accept' => 'application/json'];
    public function login()
    {
        Auth::attempt(['email' => 'admin@mail.com', 'password' => '123456789']);
        $user = Auth::user();
        $token = $user->createToken('test')->accessToken;
        return [
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ];
    }

    /**
     * Field validation when adding cars;
     */
    public function testdir()
    {
        $authHeader = $this->login();
        $response = $this->withHeaders($authHeader)->get("/api/processes/index");
        $response->assertStatus(200);
    }
}
