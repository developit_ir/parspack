<?php
/**
 * @OA\Get(
 *      path="/processes/index",
 *      tags={"Processes"},
 *      summary="display a listing of running processes on the server",
 *      description="",
 *      @OA\Response(response=200, description="Successful created", @OA\JsonContent()),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "bearerAuth": {}
 *         }
 *     },
 * )
 */
