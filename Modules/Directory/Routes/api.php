<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api', 'prefix' => '/directories'], function () {

    Route::get('/index', 'DirectoryController@index');
    Route::post('/create', 'DirectoryController@create');

    Route::group(['prefix' => '/files'], function () {

        Route::get('/index', 'FileController@index');
        Route::post('/create', 'FileController@create');
    });
});
