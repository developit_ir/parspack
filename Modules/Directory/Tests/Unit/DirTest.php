<?php

namespace Modules\Directory\Tests\Unit;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class DirTest extends TestCase
{
    public $header = ['accept' => 'application/json'];
    public function login()
    {
        Auth::attempt(['email' => 'admin@mail.com', 'password' => '123456789']);
        $user = Auth::user();
        $token = $user->createToken('test')->accessToken;
        return [
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ];
    }

    /**
     * Field validation when adding cars;
     */
    public function testdir()
    {
        $authHeader = $this->login();
        $response = $this->withHeaders($authHeader)->get("/api/directories/index");
        $response->assertStatus(200);

        $response = $this->withHeaders($authHeader)->post("/api/directories/create");
        $response->assertStatus(204);

        $response = $this->withHeaders($authHeader)->get("/api/directories/files/index");
        $response->assertStatus(200);

        $response = $this->withHeaders($authHeader)->post("/api/directories/files/create");
        $response->assertStatus(204);
    }
}
