<?php

namespace Modules\Directory\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\Directory\Entities\Directory;
use Modules\Directory\Entities\File;

class FileController extends Controller
{
    /**
     * get list of all files
     */
    public function index() : JsonResponse
    {
        $directories = Storage::disk('local')->files();
        return response()->json($directories, 200);
    }

    /**
     * create a directory with user's specified name
     */
    public function create() : JsonResponse
    {
        Storage::disk('local')->put(auth()->user()->name . '.txt', '');
        return response()->json(null, 204);
    }
}
