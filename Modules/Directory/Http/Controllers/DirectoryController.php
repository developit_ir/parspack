<?php

namespace Modules\Directory\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;

class DirectoryController extends Controller
{
    /**
     * get list of all directories
     */
    public function index() : JsonResponse
    {
        $directories = Storage::disk('local')->allDirectories();
        return response()->json($directories, 200);
    }

    /**
     * create a directory with user's specified name
     */
    public function create() : JsonResponse
    {
        Storage::disk('local')->makeDirectory(auth()->user()->name);
        return response()->json(null, 204);
    }
}
