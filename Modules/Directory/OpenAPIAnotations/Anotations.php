<?php
/**
 * @OA\Get(
 *      path="/directories/index",
 *      tags={"Directories"},
 *      summary="get list of all directories",
 *      description="",
 *      @OA\Response(response=200, description="Successful created", @OA\JsonContent()),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "bearerAuth": {}
 *         }
 *     },
 * )
 */

/**
 * @OA\Post(
 *      path="/directories/create",
 *      tags={"Directories"},
 *      summary="create a directory with user's specified name",
 *      description="",
 *      @OA\Response(response=200, description="Successful created", @OA\JsonContent()),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "bearerAuth": {}
 *         }
 *     },
 * )
 */

/**
 * @OA\Get(
 *      path="/directories/files/index",
 *      tags={"Directories/Files"},
 *      summary="get list of all files",
 *      description="",
 *      @OA\Response(response=200, description="Successful created", @OA\JsonContent()),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "bearerAuth": {}
 *         }
 *     },
 * )
 */

/**
 * @OA\Post(
 *      path="/directories/files/create",
 *      tags={"Directories/Files"},
 *      summary="create a file with user's specified name",
 *      description="",
 *      @OA\Response(response=200, description="Successful created", @OA\JsonContent()),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "bearerAuth": {}
 *         }
 *     },
 * )
 */
