<?php

namespace Modules\Directory\Entities;

use Illuminate\Database\Eloquent\Model;

class Directory extends Model
{
    const BASE_PATH = '/opt/myprogram';

    protected $fillable = [];

    /**
     * sanitaze directory name
     * @param $name
     */
    public static function sanitazeName($name) : string
    {
        return preg_replace("([^\w\s\d\-_~,;\[\]\(\)])", null, $name);
    }
}
