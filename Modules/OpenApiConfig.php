<?php
/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="OpenApi",
 *      description="Swagger OpenApi description",
 *      @OA\Contact(
 *          email="me@developit.ir"
 *      )
 * )
 */
/**
 *  @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="server"
 *  )
 */
/**
 * @OA\SecurityScheme(
 *     type="http",
 *     description="",
 *     name="Authorization",
 *     in="header",
 *     scheme="bearer",
 *     securityScheme="bearerAuth",
 *     bearerFormat="JWT"
 * )
 */
