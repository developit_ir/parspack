<p align="center">
<img src="http://developit.ir/tmp/parspack.png">
</p>


#### Install

##### A. Install dependencies via composer
``
composer update
``

##### B. Rename .env.example file to .env and update config

````
APP_URL=http://localhost
APP_DIR=parspack
L5_SWAGGER_CONST_HOST=${APP_URL}/${APP_DIR}/public/api/

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=mr.rezaee
DB_USERNAME=root
DB_PASSWORD=123
````

##### C. Migrations & Seeding 
``
php artisan migrate
``

note: nwidart laravel-modules artisan command:

``
php artisan module:seed
``

#### D. Install passport(JWT)
``
php artisan passport:install
``

#### E. re-generate swagger api documentation
``
php artisan l5-swagger:generate
``

#### Admin User
username: admin@mail.com

password: 123456789

## swagger api documentation
```
{APP_URL}/{APP_DIR}/public/api/documentation
```

#### Run tests
```
sudo php artisan test
```
